<?php

namespace App\Http\Controllers\Admin_Area;

use App\Models\QuestionnaireType;
use App\Models\QuestionnaireCategory;
use App\Services\EvaluationCriteriaService;
use Illuminate\Http\Request;

class EvaluationCriteriaController extends AdminController
{
    protected $evaluation_criteria;

    public function __construct()
    {
        parent::__construct();
        $this->evaluation_criteria = new EvaluationCriteriaService();
    }


    public function add_evaluation_criteria()
    {
        $questionnarie_types = QuestionnaireType::all();
        $questionnarie_categories = QuestionnaireCategory::all();
        return view ('admin.evaluation_criteria.add_evaluation_criteria')
            ->with('questionnarie_categories', $questionnarie_categories)
            ->with('questionnarie_types',$questionnarie_types);
    }

    public function do_add_evaluation_criteria(Request $request)
    {
        $this->validate($request, [
            'questionnaire_type' => 'required',
            'questionnaire_category' => 'required',
            'questionnaire_subcategory' => 'required',
        ]);

        $criteria['type_id'] = $request->questionnaire_type;
        $criteria['category_id'] = $request->questionnaire_category;
        $criteria['subcategory_id'] = $request->questionnaire_subcategory;

        foreach ($request->criteria as $data) {
            $criteria['name'] = $data;
            $this->evaluation_criteria->save_evaluation_criteria($criteria);
        }

        return redirect('/admin/adauga_criterii_evaluare');
    }

    public function get_evaluation_criteria_list($type_id, $category_id, $subcategory_id)
    {
        $conditions = [
            'type_id' => $type_id,
            'category_id' => $category_id,
            'subcategory_id' => $subcategory_id,
                    ];
        $data = $this->evaluation_criteria->get_rows($conditions);
       return json_encode($data);
    }

    public function get_evaluation_criteria_with_alias ($type_id, $category_id, $subcategory_id)
    {
        $data = EvaluationCriteriaService::get_all_evaluation_criteria_for_subcategory($type_id, $category_id, $subcategory_id);
        return json_encode($data);
    }

}