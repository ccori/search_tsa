<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;

abstract class Controller extends BaseController
{
    use DispatchesJobs, ValidatesRequests;
    protected $layout;
    public $base_url;
    public $asset_url;
    function __construct() {
        $this->base_url = \Config::get ( 'app.url' );
        $this->asset_url = \Config::get ( 'app.asset_url' );
    }
}
