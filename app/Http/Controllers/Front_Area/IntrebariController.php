<?php
namespace App\Http\Controllers\Front_Area;

use App\Http\Controllers\FrontEndController;
use App\Models\Question;
use App\Services\QuestionAnswerService;
use Illuminate\Http\Request;

class IntrebariController extends FrontEndController{

    public function __construct()
    {
        parent::__construct();
        $this->middleware('guest');
    }

    public function view_inrebari()
    {
        return view('front_area.view_questionnaire');
    }

    public function show_questions()
    {
        $question_answer = new QuestionAnswerService();
        $questions = Question::orderBy('ordine','asc')->get();
        foreach($questions as $question) {
            $question->answers = $question_answer->get_answers_for_question($question->id);
        }
        return json_encode($questions);
    }

    public function process_questions(Request $request)
    {
        $question_answer = new QuestionAnswerService();

        $questions_list = Question::lists('intrebare', 'id');
        $answers = $request->all();

        if(count($answers['intrebari']) < count($questions_list)){
            return \Redirect::back()->withErrors('Trebuie sa raspundeti la toate intrebarile ')->withInput($request->all());
        }
        $raspuns_important = $question_answer->get_punctaj_for_answers($request->all(),1);
        $raspuns = $question_answer->get_punctaj_for_answers($request->all());

        if($raspuns_important['punctaj_total'] >= 2 || $raspuns['punctaj_total'] >= 3){

             $message = "In urma analizarii raspunsurilor introduse in chestionar exista suspiciuni legate de autism. Va recomandam sa continuati evaluarile la un specialist psiholog/psihiatru/pediatru sau sa apelati infoline Autism Baia Mare: 0800 500 288";
        }else{
             $message = "Nu sunt suspiciuni legate de autism.";
        }

        return view('user_area.questionnaire_answer')->with('message', $message);
    }
}