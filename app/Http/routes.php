<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'FrontEndController@index');

Route::get('/vizualizeaza_chestionar', 'Front_Area\IntrebariController@view_inrebari');
Route::get('/questions', 'Front_Area\IntrebariController@show_questions');
Route::post('/proceseaza_chestionar', 'Front_Area\IntrebariController@process_questions');

Route::get('home', 'BackEndController@index');

Route::get('/pacient/adauga', 'User_Area\PacientController@create');
Route::post('/pacient/verfy_cnp/', 'User_Area\PacientController@verfy_cnp');

// Store a new pacient...
Route::post('/pacient/do_edit/{patient_id}', 'User_Area\PacientController@do_edit');
// Edit a pacient...
Route::post('/pacient/store', 'User_Area\PacientController@store');
// get towns with county...
Route::get('/pacient/get_towns/{county_id}', 'User_Area\PacientController@get_towns');
// get patients
Route::get('/pacient/list', 'User_Area\PacientController@list_patients');
// get patients
Route::get('/pacient/datatables', 'User_Area\PacientController@datatables');
// edit patients
Route::get('/pacient/edit/{pacient_id}', 'User_Area\PacientController@edit');
// detalii patients
Route::get('/pacient/detalii/{pacient_id}', 'User_Area\PacientController@detalii');

Route::get('/pacient/chestionar/{pacient_id}', 'User_Area\QuestionnaireController@show_questionnaire');

Route::post('/pacient/submit_questionnaire', 'User_Area\QuestionnaireController@submit_questionnaire');
Route::get('/pacient/vizualizare_chestionar', 'User_Area\QuestionnaireController@view_questionnaire');
Route::post('/pacient/save_question_to_pacient/{pacient_id}', 'User_Area\QuestionnaireController@save_question_to_pacient');

Route::get('/pacient/rapoarte/statistici_pacienti', 'User_Area\ReportsController@patients_statistics');
Route::get('/chestionar/aplica', 'User_Area\QuestionnaireController@alege_chestionar');
Route::get('/chestionar/aplica/{chestionar_id}', 'User_Area\QuestionnaireController@aplica_chestionar');



Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
    'user' => 'User_Area\UserController',
]);
Route::get('/questions_list/{subcategory_id}', 'Admin_Area\AdminController@show_questions');
Route::group(['middleware' => 'App\Http\Middleware\AdminMiddleware'], function()
{
    Route::get('/admin','Admin_Area\AdminController@index');
    Route::resource('/do_add_questions', 'Admin_Area\AdminController@do_add_questions');
    Route::post('/delete_question/{question_id}', 'Admin_Area\AdminController@delete_question');
    Route::get('/admin/lista_pacienti', 'Admin_Area\PatientController@list_patients');
    Route::get('/admin/lista_utilizatori', 'Admin_Area\UserController@list_users');
    Route::get('/admin/adauga_intrebari', 'Admin_Area\QuestionnaireController@add_questionnaire');

    Route::post('/do_add_questionnaire', 'Admin_Area\QuestionnaireController@do_add_questionnaire');
    Route::post('/do_add_questionnaire_subcategory', 'Admin_Area\QuestionnaireController@do_add_questionnaire_subcategory');
    Route::post('/do_add_questionnaire_category', 'Admin_Area\QuestionnaireController@do_add_questionnaire_category');
    Route::post('/do_add_questionnaire_type', 'Admin_Area\QuestionnaireController@do_add_questionnaire_type');
    Route::get('/get_subcategories', 'Admin_Area\QuestionnaireController@get_subcategories');
    Route::get('/get_questions/{type_id}/{category_id}/{subcategory_id}', 'Admin_Area\QuestionnaireController@get_questions');

    Route::get('/get_questionnaire_category', 'Admin_Area\QuestionnaireController@get_questionnaire_category');


    Route::get('/admin/adauga_criterii_evaluare', 'Admin_Area\EvaluationCriteriaController@add_evaluation_criteria');
    Route::post('/do_add_evaluation_criteria', 'Admin_Area\EvaluationCriteriaController@do_add_evaluation_criteria');
    Route::get('/get_evaluation_criteria_list/{type_id}/{category_id}/{subcategory_id}', 'Admin_Area\EvaluationCriteriaController@get_evaluation_criteria_list');
    Route::get('/get_evaluation_criteria_with_alias/{type_id}/{category_id}/{subcategory_id}', 'Admin_Area\EvaluationCriteriaController@get_evaluation_criteria_with_alias');


});

