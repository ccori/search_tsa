<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QuestionnaireSubcategory extends Model{

    protected $table = 'questionnaire_subcategory';

    public $timestamps = false;
}
