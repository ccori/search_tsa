<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Town extends Model 
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'towns';

    public $timestamps = false;

	
}
