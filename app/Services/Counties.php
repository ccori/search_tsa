<?php
namespace App\Services;

use \App\Models\County;
use \App\Dao\BasicDao;

class Counties extends BasicDao
{
	static $model = '\App\Models\County';
}