<?php
 namespace App\Services;

use \App\Models\QuestionPatient;
use Illuminate\Support\Facades\DB;

Class QuestionPatientService
{


    public function save_question_patient($data, $question_patient_id = null){

        if ($question_patient_id == null) {
            $question_patient = new QuestionPatient();
        } else {
            $question_patient = QuestionPatient::find($question_patient_id);
        }

        foreach ($data as $key => $save_data) {
            $question_patient->$key = $data[$key];
        }
        $question_patient->save();

        return $question_patient->id;

    }

    function get_answers_for_patient($patient_id, $intrebare_importanta=null){
        $query = QuestionPatient::join('questions', 'questions.id', '=', 'questions_to_pacients.intrebare_id')
            ->join('questions_answers', 'questions_answers.id', '=', 'questions_to_pacients.raspuns_id')
            ->select(DB::raw('sum(punctaj_raspuns) as punctaj_total'))
            ->where('pacient_id', '=', $patient_id);
        if($intrebare_importanta == 1){
            $query = $query -> where('intrebare_importanta', '=', 1);
        }
        $query = $query->firstOrFail();
        return $query;
    }


}
