<?php namespace App\Services;

use \App\Models\Question;
Class QuestionService
{


    public function save_question($data, $question_id = null)
    {

        if ($question_id == null) {
            $question = new Question();
        } else {
            $question = Question::find($question_id);
        }

        foreach ($data as $key => $save_data) {
            $question->$key = $data[$key];
        }
        $question->save();

        return $question->id;

    }

    public static function get_questions_for_subcategoty($type_id, $category_id, $subcategory_id)
    {
        return Question::leftJoin('evaluation_criteria', 'evaluation_criteria.id', '=', 'evaluation_criteria_id')
            ->select('questions.id', 'intrebare', 'ordine', 'questions.type_id', 'questions.category_id', 'questions.subcategory_id', 'name')
            ->where('questions.type_id',$type_id)
            ->where('questions.category_id',$category_id)
            ->where('questions.subcategory_id',$subcategory_id)
            ->orderBy('ordine','asc')
            ->get();
    }


}