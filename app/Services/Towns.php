<?php
namespace App\Services;

use \App\Models\Town;
use \App\Dao\BasicDao;

class Towns extends BasicDao
{
	static $model = '\App\Models\Town';
}