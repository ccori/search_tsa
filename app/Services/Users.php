<?php
namespace App\Services;

use App\Models\Patient;
use \App\Models\User;
use \App\Dao\BasicDao;
use Validator;

class Users extends BasicDao
{
	static $model = '\App\Models\User';
	
	public function get_user_patients($user){
		return $user->patients()->get();
	}
	
	public function get_user_pacient($user, $pacient_id){
		return $user->whereHas('patients', function ($query) use ($pacient_id) {
		    $query->where('patients.id', $pacient_id);
		})->get();
	}


	public function save_pacient($user, $request, $patient_id = null)
	{
		if ($patient_id == null) {
			$patient = new Patient();
		} else {
			$patient = $request;
		}

		foreach ($request as $key => $save_data) {
			$patient->$key = $request[$key];
		}
		$user->patients()->save($patient);

		return $patient;
	}

	public function save_user($request, $user_id)
	{
		$user = User::find($user_id);
		foreach ($request as $key => $save_data) {
			$user->$key = $request[$key];
		}
		$user->save();
		return $user;
	}
}