//var pacient_form = angular.module('pacient_form', []);
//
//pacient_form.controller('validateCtrl', function($scope, $http) {
//	$scope.submit = function(){
//	    // Set the 'submitted' flag to true
//	    $scope.submitted = true;
//	    // Send the form to server
//	    console.log($scope.create_form.$valid)
//	    if($scope.create_form.$valid){
//			$("#create_form").submit()
//	    }
//	}
//	
//	$scope.submit_first_step = function(){
//	    // Set the 'submitted' flag to true
//	    $scope.submitted = true;
//	    // Send the form to server
//	    console.log($scope.create_form.$valid)
//	    if($scope.create_form.$valid){
//	    	$('#questionnaire').show();
//			$('#show_questionnaire').hide();
//	    }
//	}
//	$scope.change_oras = function(id) {
//        $http.get("/pacient/get_towns/"+$scope.judet)
//    	.success(function(response) {
//    		$scope.choose_town=response;  
//    		console.log(response[response.map(function(x) {return x.id; }).indexOf($("#localitate").attr('initial_value'))])
//    		
//    		// $scope.localitate = response[response.map(function(x) {return x.id; }).indexOf($("#localitate").attr('initial_value'))];
//    	});
//    };
//    angular.element(document).ready(function () {
//        $scope.change_oras($scope.judet);
//    });
//});

 $(document).ready(function(){
	 
	 $.validator.addMethod( "lettersonly", function( value, element ) {
			return this.optional( element ) || /^[a-z\s]+$/i.test( value );
		}, "Campul poate contine doar litere si spatiu." );
	 $.validator.addMethod( "letterswithbasicpunc", function( value, element ) {
			return this.optional( element ) || /^[a-z\-.,()'"\s]+$/i.test( value );
		}, "Campul poate contine litere, spatiu si semne de punctuatie" );
	 $.validator.addMethod( "letterswithbasicpuncnumbers", function( value, element ) {
		 return this.optional( element ) || /^[0-9a-z\-.,()'"\s]+$/i.test( value );
	 }, "Campul poate contine litere, spatiu si semne de punctuatie" );
	 
	 $( "#pacient_details_form" ).validate({
		  rules: {
		    nume: {
		      required: true,
		      lettersonly: true
		    },
			  prenume: {
				  required: true,
				  lettersonly: true
			  },
			  cnp: {
				  required: true,
				  digits: true,
				  minlength: 13,
				  maxlength: 13,
			      remote: {
			          url: "/pacient/verfy_cnp/",
			          type: "post"
			      }

			  },
			  adresa: {
				  required: true,
				  letterswithbasicpuncnumbers: true
			  },
			  telefon: {
				  required: true,
				  digits: true
			  }
		  }, 
	        messages: {
	        	cnp: {
	                remote: "CNP existent"
	            }  
	        }
	});
	 $("#judet").change(function(){
		 $.ajax({
			  url: "/pacient/get_towns/"+$(this).val()
			}).done(function(response) {
				 obj = jQuery.parseJSON(response);
				 $("#localitate > option:gt(0)").remove()
				 $.each(obj, function(index,item){
					 $("#localitate").append('<option value="'+item.id+'">'+item.name+'</option>')
				 })
				
			    
			});
	 })
	 
	 $(".buton_next").click(function(e){
		 e.preventDefault()
		 if($(this).attr("data-pas") == 0){
			 save_pacient_data();
		 }else{
			 save_question(this);
		 }
	 })
 })
 
 function save_pacient_data(){
	 if($("#pacient_details_form").valid()){
		 $.ajax({
			 url: "/pacient/submit_questionnaire",
			 method: "POST",
			 data: $('#pacient_details_form').serialize()
		 }).done(function(response){
			 obj = jQuery.parseJSON(response);
			 
			 $("#pacient_id").val(obj.id)
			 $("#pacient_details").hide()
			 $("#chestionar_2").fadeIn()
			 
			 $(".pasul[data-pas=1]").removeClass("active").addClass("complete")
			 $(".pasul[data-pas=2]").removeClass("disabled").addClass("active")
		 })
	 }
 }
 
 function save_question(button){
	 if($("#chestionar_form_"+$(button).attr("data-pas")).valid()){
		 $.ajax({
			 url: "/pacient/save_question_to_pacient/"+$("#pacient_id").val(),
			 method: "POST",
			 data: $("#chestionar_form_"+$(button).attr("data-pas")).serialize()
		 }).done(function(response){
			 
			 $("#chestionar_"+$(button).attr("data-pas")).hide()
			 $("#chestionar_"+(parseInt($(button).attr("data-pas"))+1)).fadeIn()
			 
			 $(".pasul[data-pas="+$(button).attr("data-pas")+"]").removeClass("active").addClass("complete")
			 $(".pasul[data-pas="+(parseInt($(button).attr("data-pas"))+1)+"]").removeClass("disabled").addClass("active")
		 })
	 }
 }