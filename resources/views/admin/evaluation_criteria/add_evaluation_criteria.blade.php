@extends('layouts.app')

@section('content')

    <style>
        #form label{
            display: inline-block;
            float: left;
            clear: left;
            width: 150px;
            text-align: right;
            margin-right: 10px;
            margin-top: 10px;
        }
        #form select, textarea {
            margin-top: 10px;
            width: 450px;
        }

        #form input {
            margin-top: 10px;
            width: 50px;
        }

        #form p {
            display: inline-block;
            float: left;
            margin-top: 10px;
            color: red;
        }

        #form input[type="submit"]{
            margin-top: 18px;
            width: 450px;
        }
        #form input[type="button"]{
            margin-top: 18px;
            width: 450px;
        }
        .selectize-input {
            width: 450px !important;
        }

        span.raspuns {
            width: 400px;
            float:left;
            margin-left:10px;
            margin-bottom: 10px;
        }

        div.intrebare {
            float: left;
        }

    </style>


    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Adauga criterii de evaluare</div>
                    <div class="panel-body">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li> @endforeach
                                </ul>
                            </div>
                        @endif

                        <form id="form" method="post" action="{{ url() }}/do_add_evaluation_criteria">

                            <div class="form-group">
                                <label class="control-label">Tip Chestionar : </label>
                                <select name="questionnaire_type" id ="questionnaire_type">
                                    <option value=""></option>
                                    @foreach($questionnarie_types as $questionnarie_type)
                                        <option value={{$questionnarie_type->id}}> {{$questionnarie_type->questionnaire_type_name}}</option>
                                    @endforeach
                                </select></br>
                                <label class="control-label">Categorie Chestionar : </label>
                                <select name="questionnaire_category" id ="questionnaire_category">
                                    <option value=""></option>
                                    @foreach($questionnarie_categories as $questionnarie_category)
                                        <option value={{$questionnarie_category->id}}> {{$questionnarie_category->category_name}}</option>
                                    @endforeach
                                </select></br>
                                <label class="control-label">Subcategorie Chestionar : </label>
                                <select name="questionnaire_subcategory" id ="questionnaire_subcategory">
                                    <option value=""></option>
                                </select></br>
                                <label class="control-label">Denumire criteriu evaluare : </label>
                                <textarea  id="criteria[1]" name="criteria[1]" cols="40%"></textarea>
                                <div class = 'add_area'></div>
                                <label> </label><input class="btn btn-info btn-lg " type="button" id='row' value="Adauga noi criterii de evaluare in categorie"/>
                            </div>

                            <label> </label><input class="btn btn-success btn-lg btn-block" type="submit" id="submit" value="Submit"/>
                        </form>
                    </div>
                    <div class="panel-heading">Lista criterii de evaluare</div>
                    <div class="panel-body">
                        <div>
                            <div class="evaluation_criteria_list" >
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function () {
            var max_fields = 100; //maximum input boxes allowed
            var wrapper = $(".add_area"); //Fields wrapper
            var add_conditions = $("#row"); //Add button ID


            var x = 1; //initlal text box count

            $(add_conditions).on('click', function (e) { //on add input button click
                e.preventDefault();
                if (x < max_fields) { //max input box allowed
                    x++; //text box increment
                    $(wrapper).append('<div> <label class="control-label">Denumire criteriu evaluare : </label>' +
                    '<textarea name="criteria[' + x + ']"  name="criteria['+ x + ']" cols="40%"></textarea>' +
                    '</span>&nbsp;<a href="#" class="remove_field">Remove</a><div>');
                }
            });


            $(wrapper).on("click", ".remove_field", function (e) { //user click on remove text
                e.preventDefault();
                $(this).parent('div').remove();
                x--;

            });
            $('select#questionnaire_category').selectize({
                plugins: ['restore_on_backspace'],
                delimiter: ',',
                placeholder: 'Selectati o optiune',

                create: false
            });

            $('select#questionnaire_type').selectize({
                plugins: ['restore_on_backspace'],
                delimiter: ',',
                placeholder: 'Selectati o optiune',

                create: false
            });


            $('#questionnaire_type,#questionnaire_category').change(function() {
                $.ajax({
                    url: "/get_subcategories",
                    data: {category_id: $('#questionnaire_category').val(), type_id: $('#questionnaire_type').val()},
                    type: 'GET',
                    success: function (data) {
                        $('#questionnaire_subcategory').empty().append('<option selected="selected" value="">Please select one option</option>');
                        data = $.parseJSON(data);
                        _selectize[0].selectize.clear();
                        _selectize[0].selectize.clearOptions();
                        _selectize[0].selectize.addOption(data);
                    }
                });
            });


            var _selectize = $('select#questionnaire_subcategory').selectize({
                plugins: ['restore_on_backspace'],
                delimiter: ',',
                placeholder: 'Selectati intai tipul si categoria ',

                create: false
            });

            $('#questionnaire_type,#questionnaire_category,#questionnaire_subcategory').change(function() {
                type_id = $('#questionnaire_type').val();
                category_id = $('#questionnaire_category').val();
                subcategory_id = $('#questionnaire_subcategory').val();
                if(type_id != '' && category_id !='' && subcategory_id !='') {
                    $('.evaluation_criteria_list').empty();
                    $.ajax({
                        url: "/get_evaluation_criteria_list/" + type_id + "/" + category_id + "/" + subcategory_id ,
                        type: 'GET',
                        success: function (data) {
                            data = $.parseJSON(data);

                            $.each(data, function(key, value){

                                $('.evaluation_criteria_list').append( "<div class = 'evaluation_criteria' id='criteria[" + key + "]'>" +
                                        value.name +
                                      "</div>"
                                );
                            });
                        }
                    });
                }

            });
        });

    </script>
@endsection
