@extends('layouts.app') @section('content')
<script data-require="angular-messages@*" data-semver="1.4.3" src="https://code.angularjs.org/1.4.3/angular-messages.min.js"></script>
<script type="text/javascript" src="/js/ngRemoteValidate.0.6.1.min.js"></script>
<script src="/js/user_area/create_pacient.js"></script> 
 

<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Lista Pacienti</div>
				
				<div class="panel-body">

						
						<div class="form-group">
                        <table width="100%" class="table table-striped">
                        <tr>
                            <th>Nume</th>
                            <th>CNP</th>
                            <th>Email</th>
                            <th>Suspect autism</th>
                            <th>Data testarii</th>
                            <th>Test aplicat de</th>
                            <th></th>
                        </tr>
                        @foreach ($patients as $patient)
							<tr>
                                <td>{{ $patient->nume }} {{ $patient->prenume}}</td>
                                <td>{{ $patient->cnp}}</td>
                                <td>{{ $patient->email }}</td>
                                <td>@if( $patient->posibil_autism  == 1 )Da @else Nu @endif</td>
                                <td>{{$patient->tested_at}}</td>
                                <td>{{$patient->name}}</td>
                                <td><a href="/pacient/detalii/{{ $patient->id }}"><span class = "glyphicon glyphicon glyphicon-check text-success">Detalii</span></a></td>
                            </tr>
						@endforeach
							</table>	
						</div>

				</div>
			</div>
		</div>
	</div>
</div>
@endsection
