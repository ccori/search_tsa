@extends('layouts.app') @section('content')
<script data-require="angular-messages@*" data-semver="1.4.3" src="https://code.angularjs.org/1.4.3/angular-messages.min.js"></script>
<script type="text/javascript" src="/js/ngRemoteValidate.0.6.1.min.js"></script>
<script src="/js/user_area/create_pacient.js"></script>


<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Lista Utilizatori</div>

                <div class="panel-body">


                    <div class="form-group">
                        <table width="100%" class="table table-striped">
                            <tr>
                                <th>Nume</th>
                                <th>Email</th>
                                <th>Tip utilizator</th>
                                <th>Utilizator din data </th>
                            </tr>
                            @foreach ($users as $user)
                            <tr>
                                <td>{{ $user->name }} </td>
                                <td>{{ $user->email}}</td>
                                <td>{{ $user->user_type }}</td>
                                <td>{{$user->created_at}}</td>
                                {{--<td><a href="/pacient/detalii/{{ $patient->id }}"><span class = "glyphicon glyphicon glyphicon-check text-success">Detalii</span></a></td>--}}
                            </tr>
                            @endforeach
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
