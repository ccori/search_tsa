@extends('layouts.app') @section('content')
<script data-require="angular-messages@*" data-semver="1.4.3" src="https://code.angularjs.org/1.4.3/angular-messages.min.js"></script>
<script type="text/javascript" src="/js/ngRemoteValidate.0.6.1.min.js"></script>
<script src="/js/user_area/create_pacient.js"></script>


<div class="container"
	 ng-app="pacient_form">
	<form id="create_form" action="/pacient/submit_questionnaire/" method="post"
		  ng-controller="validateCtrl" name="create_form"
		  novalidate>
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Adauga Chestionar</div>
				
				<div class="panel-body">

						
						<div class="form-group">
							<table style="width: 100%">
							<tr>
						@foreach ($questionaires as $questionaire)
							<td style="padding:10px; width: {{ $width }}%">
								<a class="btn btn-primary btn-info btn-lg btn-block" href="/chestionar/aplica/{{ $questionaire->id }}">{{ $questionaire->questionnaire_type_name }}</a>
							</td> 
						@endforeach
						</tr>
						</table>
					</ul>
				</div>

			</div>
		</div>
	</div>
	@yield('questionnaire')
	<input type="hidden" name="first_step" id="first_step" ng-model="first_step" value="0">
 </form>
</div>


@overwrite
