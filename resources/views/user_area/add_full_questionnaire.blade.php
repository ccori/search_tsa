@extends('layouts.app') @section('content')
<style>
<!--
.bs-wizard {margin-top: 40px;}

/*Form Wizard*/
.bs-wizard {border-bottom: solid 1px #e0e0e0; padding: 0 0 10px 0;}
.bs-wizard > .bs-wizard-step {padding: 0; position: relative;}
.bs-wizard > .bs-wizard-step + .bs-wizard-step {}
.bs-wizard > .bs-wizard-step .bs-wizard-stepnum {color: #595959; font-size: 16px; margin-bottom: 5px;}
.bs-wizard > .bs-wizard-step .bs-wizard-info {color: #999; font-size: 14px;}
.bs-wizard > .bs-wizard-step > .bs-wizard-dot {position: absolute; width: 30px; height: 30px; display: block; background: #fbe8aa; top: 45px; left: 50%; margin-top: -15px; margin-left: -15px; border-radius: 50%;} 
.bs-wizard > .bs-wizard-step > .bs-wizard-dot:after {content: ' '; width: 14px; height: 14px; background: #fbbd19; border-radius: 50px; position: absolute; top: 8px; left: 8px; } 
.bs-wizard > .bs-wizard-step > .progress {position: relative; border-radius: 0px; height: 8px; box-shadow: none; margin: 20px 0;}
.bs-wizard > .bs-wizard-step > .progress > .progress-bar {width:0px; box-shadow: none; background: #fbe8aa;}
.bs-wizard > .bs-wizard-step.complete > .progress > .progress-bar {width:100%;}
.bs-wizard > .bs-wizard-step.active > .progress > .progress-bar {width:50%;}
.bs-wizard > .bs-wizard-step:first-child.active > .progress > .progress-bar {width:0%;}
.bs-wizard > .bs-wizard-step:last-child.active > .progress > .progress-bar {width: 100%;}
.bs-wizard > .bs-wizard-step.disabled > .bs-wizard-dot {background-color: #f5f5f5;}
.bs-wizard > .bs-wizard-step.disabled > .bs-wizard-dot:after {opacity: 0;}
.bs-wizard > .bs-wizard-step:first-child  > .progress {left: 50%; width: 50%;}
.bs-wizard > .bs-wizard-step:last-child  > .progress {width: 50%;}
.bs-wizard > .bs-wizard-step.disabled a.bs-wizard-dot{ pointer-events: none; }
/*END Form Wizard*/
-->
span.raspuns {
	width: 400px;
	float:left;
	margin-left:10px;
	margin-bottom: 10px;
}
.question_row:nth-of-type(even) {
	background: #D9EDF7;
}
</style>
<div class="container">
<input type="hidden" id="pacient_id" value="0">
	<div class="row bs-wizard" style="border-bottom:0;">
                
                <div class="col-xs-3 bs-wizard-step active pasul" style="width: {{ $step_width }}%" data-pas="1">
                  <div class="text-center bs-wizard-stepnum">Pasul 1</div>
                  <div class="progress"><div class="progress-bar"></div></div>
                  <a href="#" class="bs-wizard-dot"></a>
                  <div class="bs-wizard-info text-center"></div>
                </div>
                
                @foreach ($questionnaire_subcategories as $key=>$questionnaire_subcategory)
                    <div class="col-xs-3 bs-wizard-step disabled pasul" style="width: {{ $step_width }}%" data-pas="{{ $key+2 }}"><!-- complete -->
                      <div class="text-center bs-wizard-stepnum">Pasul {{ $key+2 }}</div>
                      <div class="progress"><div class="progress-bar"></div></div>
                      <a href="#" class="bs-wizard-dot"></a>
                      <div class="bs-wizard-info text-center"></div>
                    </div>
                @endforeach
                
                <div class="col-xs-3 bs-wizard-step disabled pasul" style="width: {{ $step_width }}%" data-pas="{{ $subcat_nr }}">
                  <div class="text-center bs-wizard-stepnum">Pasul {{ $subcat_nr }}</div>
                  <div class="progress"><div class="progress-bar"></div></div>
                  <a href="#" class="bs-wizard-dot"></a>
                  <div class="bs-wizard-info text-center"></div>
                </div>
        
        
        
        
        
	</div>
	@include('user_area.create_pacient',$data_pacient)
	@foreach ($questionnaire_subcategories as $key=>$questionnaire_subcategory)
		<form id="chestionar_form_{{ $key+2 }}">
        	<div class="row" style="display: none" id="chestionar_{{ $key+2 }}">
        		<div class="col-md-10 col-md-offset-1">
        			<div class="panel panel-default">
        				<div class="panel-heading">{{ $questionnaire_subcategory->category_name }} - {{ $questionnaire_subcategory->subcategory_name }}</div>
        				<div class="panel-body">
        
        						@foreach ($questionnaire_subcategory['questions'] as $question)
        						<div class="form-group question_row">
            							<div class="control-label">{{ $question->intrebare }}</div>
            							<div class="">
											<span class ='raspuns'> DA <input type="radio" class="" name="intrebari[{{ $question->id }}]" required value="1"> </span>
            								<span class ='raspuns'> NU <input type="radio" class="" name="intrebari[{{ $question->id }}]" value="0"> </span>
            							</div>
									<label style="display: none" id="intrebari[{{ $question->id }}]-error" class="error" for="intrebari[{{ $question->id }}]">Acest câmp este obligatoriu.</label></br>
        						</div>
        						@endforeach
							<div class="col-md-10">
								<button class="btn btn-primary btn-block buton_next" data-pas="{{ $key+2 }}" data-subcat_id="{{ $questionnaire_subcategory->id }}" type="button">Continua</button>
							</div>
        				</div>
        			</div>
        		</div>
        	</div>	
        </form>
	@endforeach
	<div class="row" style="display: none" id="chestionar_{{ $key+2 }}">
        		<div class="col-md-10 col-md-offset-1">
        			<div class="panel panel-default">
        				<div class="panel-heading">Rezultat chestionar</div>
        				<div class="panel-body">
        
        						cucu lului
        				</div>
        			</div>
        		</div>
        	</div>
</div>


@overwrite
