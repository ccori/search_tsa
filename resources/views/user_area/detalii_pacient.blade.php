@extends('layouts.app') @section('content')
<script data-require="angular-messages@*" data-semver="1.4.3" src="https://code.angularjs.org/1.4.3/angular-messages.min.js"></script>
<script type="text/javascript" src="/js/ngRemoteValidate.0.6.1.min.js"></script>
<script src="/js/user_area/create_pacient.js"></script> 
<style>
ul{
    list-style: none outside none;
}

.detalii span{
    font-weight: bold;
}
</style>

<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Detalii Pacient</div>

				<div class="panel-body">
                    <ul class="detalii">
						<li>
							<span class="col-md-6 control-span ">Nume</span>
							<div class="col-md-6">
								{{ $patient->nume }}
							</div>
                        </li>
                        <li>
							<span class="col-md-6 control-span">Prenume</span>
							<div class="col-md-6">
								<div>{{ $patient->prenume }}</div>
							</div>
						</li>
                        <li>
                        	<span class="col-md-6 control-span">CNP</span>
							<div class="col-md-6">
								<div>{{ $patient->cnp }}</div>
							</div>
						</li>
                        <li>
                        	<span class="col-md-6 control-span">Nume Mama</span>
							<div class="col-md-6">
                                <div>{{ $patient->mama }}</div>
							</div>
						</li>
                        <li>
                        	<span class="col-md-6 control-span">Nume Tata</span>
							<div class="col-md-6">
                                    <div>{{ $patient->tata }}</div>
							</div>
						</li>
                        <li>
                        	<span class="col-md-6 control-span">Adresa</span>
							<div class="col-md-6">
								<div>{{ $patient->adresa }}</div>
							</div>
						</li>
                        <li>	
							<span class="col-md-6 control-span">Judet</span>
							<div class="col-md-6">
								
								<div>{{ $patient->judet }}</div>
							</div>
						</li>
                        <li>
                            <span class="col-md-6 control-span">Localitate</span>
							<div class="col-md-6">
								<div>{{ $patient->localitate }}</div>
							</div>
						</li>
                        <li>
                        	<span class="col-md-6 control-span">Email</span>
							<div class="col-md-6">
								<div>{{ $patient->email }}</div>
							</div>
						</li>
                        <li>
                        	<span class="col-md-6 control-span">Telefon</span>
							<div class="col-md-6">
								<div>{{ $patient->telefon }}</div>
							</div>
						</li>
                        <li><div>
                        	<span class="col-md-8 control-span">Observatii Medicale</span>
							<div class="col-md-6">
                                    {{ $patient->observatii_medicale }}
							</div>
							</div>
						</li>
                        <li>
                        	<span class="col-md-6 control-span">Suspect autism</span>
							<div class="col-md-6">
                                    <div>@if( $patient->posibil_autism  == 1 )Da @else Nu @endif</div>
							</div>
						</li>
                        <li>
                        	<span class="col-md-6 control-span">Data testului</span>
							<div class="col-md-6">
                                    <div>{{ $patient->tested_at }}</div>
							</div>
						</li>

					</ul>		

				</div>
                
			</div>
                <div class="panel panel-default">
                <div class="panel-body">
                <ul>
                <li>
                    <div class="btn-group" role="group" aria-label="...">
                            <a href='/pacient/chestionar/{{ $patient->id }}'><button type="button" class="btn  btn-info">Aplica chestionar</button></a>
                            <a href='/pacient/edit/{{ $patient->id }}'><button type="button" class="btn btn-warning">Editeaza Pacient</button></a>
                    </div>
                </li>
                    </ul>
                </div>
                </div>
                
		</div>
	</div>
</div>
@endsection
