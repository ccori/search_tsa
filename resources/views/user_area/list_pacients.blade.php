@extends('layouts.app') @section('content')
<script data-require="angular-messages@*" data-semver="1.4.3" src="https://code.angularjs.org/1.4.3/angular-messages.min.js"></script>
<script type="text/javascript" src="/js/ngRemoteValidate.0.6.1.min.js"></script>
<script type="text/javascript" src="//code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="/js/user_area/create_pacient.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css">
<script src="//cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script> 
 

<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Lista Pacienti</div>
				
				<div class="panel-body">

						
						<div class="form-group">
                        <table width="100%" class="table table-striped" id="myTable">
                        <thead>
                        <tr>
                            <th>Nume</th>
                            <th>CNP</th>
                            <th>Email</th>
                            <th>Suspect autism</th>
                            <th>Data testarii</th>
                            <th></th>
                        </tr>
                        </thead>
                        <!--  <tbody>
                      
						</tbody>
						-->
                        <tfoot>
                            <th>Nume</th>
                            <th>CNP</th>
                            <th>Email</th>
                            <th>Suspect autism</th>
                            <th>Data testarii</th>
                            <th></th>
                        </tr>
                        </tfoot>
							</table>	
						</div>

				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">$(document).ready(function(){
    $('#myTable').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": "/pacient/datatables",
        "columns": [
                    { 
                        "data": null,
                    	"render" : function ( data, type, full ) {
                            return full['nume']+' '+full['prenume'];
                        }
                    },
                    { "data": "cnp" },
                    { "data": "email" },
                    { "data": "posibil_autism" },
                    { "data": "tested_at" },
                    {
                        "class": 'details-control',
                        "orderable": false,
                        "data": null,
                        "defaultContent": '',
                        "render" : function ( data, type, full ) {
                            return '<a href="/pacient/edit/'+full['id']+'"><span class = "glyphicon glyphicon-edit text-primary">Editeaza </span></a>&nbsp;&nbsp;&nbsp;'
                        	+'<a href="/pacient/detalii/'+full['id']+'"><span class = "glyphicon glyphicon glyphicon-check text-success">Detalii</span></a>';
                        }
                    }
                ]
    });
});
                        
<!--

//-->
</script>
@endsection
